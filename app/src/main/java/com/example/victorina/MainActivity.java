package com.example.victorina;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

public class MainActivity extends AppCompatActivity {
    private static final int TOTAL_LIFE = 3;
    private static ArrayMap<String, Boolean> questions = new ArrayMap<>();

    private int currentLife = TOTAL_LIFE;
    private int qtyOfQuestions;
    private int currentQuestionOrder = 0;
    private String currentQuestion;
    private Toast answerInfo;
    private int rightAnswers;
    private int wrongAnswers;

    public TextSwitcher questionBlock;
    public TextView questionNumberBlock;
    public LinearLayout healthBarLayout;
    public HearthIcon hearthIcon = new HearthIcon(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        questionBlock = findViewById(R.id.questionTextView);
        questionNumberBlock = findViewById(R.id.questionNumber);
        healthBarLayout = findViewById(R.id.healthbarLinearLayout);

        Animation slideInLeftAnimation = AnimationUtils.loadAnimation(this,
                android.R.anim.slide_in_left);
        Animation slideOutRightAnimation = AnimationUtils.loadAnimation(this,
                android.R.anim.slide_out_right);

        questionBlock.setInAnimation(slideInLeftAnimation);
        questionBlock.setOutAnimation(slideOutRightAnimation);

        questionBlock.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                TextView textView = (TextView) inflater.inflate(R.layout.question_in_switcher, null);
                return textView;
            }
        });

        renderHearts();
        generateQuestions();
        setNewQuestion();
        instructionDialog();
    }

    private void renderHearts() {
        healthBarLayout.removeAllViews();


        for (int i = 0; i < currentLife; i++) {
            hearthIcon.createRed(healthBarLayout);
        }
        for (int i = 0; i < TOTAL_LIFE - currentLife; i++) {
            hearthIcon.createEmpty(healthBarLayout);
        }
        if (currentQuestionOrder != 0) {
            View lastHearth = healthBarLayout.getChildAt(currentLife);
            AlphaAnimation animation1 = new AlphaAnimation(0.0f, 1.0f);
            lastHearth.setAlpha(1.0f);
            animation1.setDuration(2000);
            animation1.setStartOffset(0);
            lastHearth.startAnimation(animation1);
        }

    }

    private void generateQuestions() {
        questions.put("Отец языка программирования Ruby Юкихито Мацумото?", true);
        questions.put("Биткоин придумал Сатоси Накомото?", true);
        questions.put("Утконос это птица?", false);
        questions.put("Америка это первая страна выведшая спутник на орбиту земли?", false);
        questions.put("Руслан и Людмила написал Есенин?", false);
        questions.put("Прадед Пушкина был чернокожий?", true);
        questions.put("Лучшие тусовки у нас в клубе?", true);
        questions.put("Алексей лучший препод в мире (после Паши естветсвенно)?", true);
        questions.put("Android это прикольно?", true);
        questions.put("В Golang добавят дженерики?", false);
        questions.put("Коня Александра Македонского звали Искандер?", false);
        questions.put("В ближневосточных странах Александра Македонского называли Буцефал?", false);
        qtyOfQuestions = questions.size();
    }


    public void checkTrueAnswer(View v) {
        Boolean answer = questions.get(currentQuestion);

        if (answer) {
            rightAnswers++;
            showToastWithMessage("Правильный ответ!");
            setNewQuestion();
        } else {
            wrongAnswers++;
            showToastWithMessage("Неправильный ответ!");
            reduceCurrentLife();
            setNewQuestion();
        }
    }

    public void checkFalseAnswer(View v) {
        Boolean answer = questions.get(currentQuestion);

        if (!answer) {
            rightAnswers++;
            showToastWithMessage("Правильный ответ!");
            setNewQuestion();
        } else {
            wrongAnswers++;
            showToastWithMessage("Неправильный ответ!");
            reduceCurrentLife();
            setNewQuestion();
        }
    }

    private void showToastWithMessage(String message) {
        if (answerInfo != null) {
            answerInfo.cancel();
        }
        answerInfo = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        answerInfo.show();
    }

    private void reduceCurrentLife() {
        currentLife -= 1;
        renderHearts();
        checkLifeAndShowDialogIfLose();
    }

    private void instructionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        AlertDialog alert = builder
                .setTitle("Инструкция")
                .setMessage("Вам будут задавать увлекательные вопросы. \nОтвечайте Правда или Ложь")
                .setPositiveButton("Поехали!", null)
                .create();
        alert.show();
    }

    private void checkLifeAndShowDialogIfLose() {
        if (currentLife <= 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            AlertDialog alert = builder
                    .setTitle("Вы проиграли")
                    .setMessage("Хотите сыграть ещё?")
                    .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            recreate();
                        }
                    })
                    .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .create();
            alert.show();
        }
    }

    private void setNewQuestion() {
        if (currentQuestionOrder >= qtyOfQuestions) {
            String message = String.format("Статистика:\nКоличество правильных ответов: %s\nКоличество неправильных овтетов: %s", rightAnswers, wrongAnswers);
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            AlertDialog alert = builder
                    .setTitle("Вы ответили на все вопросы")
                    .setMessage(message)
                    .setPositiveButton("Сыграть ещё", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            recreate();
                        }
                    })
                    .setNegativeButton("Выйти", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .create();
            alert.show();
        } else {
            questionNumberBlock.setText(String.format("%s / %s", currentQuestionOrder + 1, qtyOfQuestions));
            currentQuestion = questions.keyAt(currentQuestionOrder);
            questionBlock.setText(String.format("%s", currentQuestion));
            currentQuestionOrder++;
        }
    }

}
