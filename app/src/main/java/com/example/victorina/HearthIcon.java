package com.example.victorina;

import android.app.Activity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.DrawableRes;


public class HearthIcon {
    ImageView imgView;
    LinearLayout Layout;
    Activity activity;

    public HearthIcon(Activity activity) {
        this.activity = activity;
    }

    public void createRed(LinearLayout layout) {
        commonHearthSettings(layout, R.drawable.ic_heart_red);
    }

    public void createEmpty(LinearLayout layout) {
        commonHearthSettings(layout, R.drawable.ic_heart_empty);
    }

    private void commonHearthSettings(LinearLayout layout, @DrawableRes int resId) {
        Layout = layout;
        imgView = new ImageView(activity);
        imgView.setImageResource(resId);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dpToPx(50), dpToPx(50));
        params.setMargins(0, 0, 5, 0);
        imgView.setLayoutParams(params);
        Layout.addView(imgView);
    }

    private int dpToPx(int dp) {
        float density = this.activity.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }
}
